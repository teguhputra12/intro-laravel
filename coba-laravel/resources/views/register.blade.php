<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 500px;
            margin: 50px auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
        }
        h1, h2 {
            text-align: center;
        }
        label {
            font-weight: bold;
        }
        input[type="text"],
        select,
        textarea {
            width: 100%;
            padding: 8px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        input[type="radio"],
        input[type="checkbox"] {
            margin-right: 5px;
        }
        button {
            background-color: #007bff;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }
        button:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Buat Account Baru</h1>
        <h2>Sign Up Form</h2>
        <form method="POST" action="{{ url('/register') }}">
            @csrf
            <div>
                <label for="first_name">First Name:</label>
                <br>
                <input type="text" name="first_name" id="first_name">
            </div>
            <div>
                <label for="last_name">Last Name:</label>
                <br>
                <input type="text" name="last_name" id="last_name">
            </div>
            <br>
            <div>
                <label>Gender:</label><br>
                <input type="radio" name="gender" value="male">Male
                <input type="radio" name="gender" value="female">Female
                <input type="radio" name="gender" value="other">Other
            </div>
            <br>
            <div>
                <label for="nationality">Nationality:</label>
                <br>
                <select name="nationality" id="nationality">
                    <option value="indonesia">Indonesia</option>
                    <option value="jepang">Jepang</option>
                    <option value="saudi_arabia">Saudi Arabia</option>
                </select>
            </div>
            <div>
                <p>Languages Spoken:</p>
                <input type="checkbox" name="languages[]" value="bahasa_indonesia">Bahasa Indonesia<br>
                <input type="checkbox" name="languages[]" value="english">English<br>
                <input type="checkbox" name="languages[]" value="other">Other<br>
            </div>
            <div>
                <p>Bio:</p>
                <textarea name="bio" id="bio" cols="30" rows="5"></textarea>
            </div>
            <div>
                <button type="submit">Kirim</button>
            </div>
        </form>
    </div>
</body>
</html>
